const mongoose = require("mongoose");

let MONGOURI;
if (process.env.NODE_ENV !== "production") {
  MONGOURI = "mongodb://localhost:27017/node-auth";
} else {
  MONGOURI = `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@cluster0-rp24n.mongodb.net/signup`;
}

const InitiateMongoServer = async () => {
  try {
    await mongoose.connect(MONGOURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    console.log("Connected to DB !!");
  } catch (e) {
    console.log(e);
    throw e;
  }
};

module.exports = InitiateMongoServer;

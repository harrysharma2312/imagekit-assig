exports.dateModify = function(start) {
  const currentdate = new Date(new Date().toISOString().split("T")[0]);
  const tomorrow = new Date(new Date().toISOString().split("T")[0]);
  tomorrow.setDate(tomorrow.getDate() + 1);
  let datetime;
  if (start) {
    datetime =
      currentdate.getFullYear() +
      "-" +
      ("0" + (currentdate.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + currentdate.getDate()).slice(-2);
  } else {
    datetime =
      tomorrow.getFullYear() +
      "-" +
      ("0" + (tomorrow.getMonth() + 1)).slice(-2) +
      "-" +
      ("0" + tomorrow.getDate()).slice(-2);
  }

  return new Date(datetime).toISOString();
};

const express = require("express");
const { check, validationResult } = require("express-validator");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const router = express.Router();
const helper = require("../helpers");

const User = require("../model/User");

router.post(
  "/signup",
  [
    check("name", "Please Enter a Valid Name")
      .not()
      .isEmpty(),
    check("email", "Please enter a valid email").isEmail(),
    check("password", "Please enter a valid password").isLength({
      min: 6
    })
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array()
      });
    }

    let ip = await User.find({
      ipadress: req.connection.remoteAddress,
      createdAt: { $gte: helper.dateModify(true), $lt: helper.dateModify() }
    });

    if (ip && ip.length >= 3 && !req.body.captcha) {
      return res.status(400).json({
        msg: "Please verify recaptcha first.",
        captcha: true
      });
    }

    const { name, email, password } = req.body;
    try {
      let user = await User.findOne({
        email
      });
      if (user) {
        return res.status(400).json({
          msg: "User Already Exists"
        });
      }

      user = new User({
        name,
        email,
        password,
        ipadress: req.connection.remoteAddress
      });

      const salt = await bcrypt.genSalt(10);
      user.password = await bcrypt.hash(password, salt);

      await user.save();

      const payload = {
        user: {
          id: user.id
        }
      };

      jwt.sign(
        payload,
        "randomString",
        {
          expiresIn: 10000
        },
        (err, token) => {
          if (err) throw err;
          res.status(200).json({
            token,
            msg: "Signup succesful"
          });
        }
      );
    } catch (err) {
      logger.error(err.message);
      res.status(500).send("Error in Saving");
    }
  }
);

module.exports = router;

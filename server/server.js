const express = require("express");
const bodyParser = require("body-parser");
const user = require("./routes/user");
const InitiateMongoServer = require("./config/db");
const path = require("path");
import logger from "../utils/logger";

InitiateMongoServer();

const app = express();

const PORT = process.env.PORT || 4000;

app.use("/dist", express.static("dist"));
app.use(express.static("public"));

app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.json({ message: "API Working" });
});

app.use("/user", user);

app.get("*", function(req, res) {
  res.sendFile(path.join(__dirname, "../public/index.html"), function(err) {
    if (err) {
      logger.error(
        "error-2 for API At " + getDateTime() + " => " + req.originalUrl
      );
      res.status(500).send(err);
    }
  });
});

process.on("unhandledRejection", function(err) {
  logger.error("UnhandledRejection error" + err);
});
process.on("uncaughtException", function(error) {
  logger.error("uncaughtException" + error);
  logger.error("Error Stack", error.stack);
});

app.listen(PORT, (req, res) => {
  console.log(`Server Started at PORT ${PORT}`);
});

import React, { Component } from "react";
import { hot } from "react-hot-loader";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import { routes } from "./routes";

class App extends Component {
  render() {
    return (
      <BrowserRouter data-test="AppTest">
        <Switch>
          {routes.map((v, k) => (
            <Route
              key={k}
              exact={v.exact}
              path={v.path}
              component={v.component}
            />
          ))}
        </Switch>
      </BrowserRouter>
    );
  }
}

export default hot(module)(App);

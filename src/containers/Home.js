import React, { useState } from "react";
import {
  Avatar,
  Button,
  Container,
  TextField,
  makeStyles,
  Typography,
  CssBaseline,
  LinearProgress
} from "@material-ui/core";
import { postSignupApi } from "../api";
import Notify from "../components/Notify";
import Recaptcha from "react-recaptcha";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

export default function SignIn() {
  const classes = useStyles();
  const initialState = { name: "", email: "", password: "" };
  const [state, setState] = useState({ ...initialState });
  const [notifier, setNotifier] = useState({ loading: false, message: "" });
  const [captcha, setCaptcha] = useState(false);
  const [verify, setVerify] = useState(false);
  const { name, email, password } = state;

  const handleChange = e => {
    const value =
      ["email", "password"].indexOf(e.target.name) > -1
        ? e.target.value.trim()
        : e.target.value;
    setState({ ...state, [e.target.name]: value });
  };

  const handleSubmit = () => {
    setNotifier({ loading: true, message: "" });
    if (name.trim() && email.trim() && password.trim()) {
      postSignupApi({ ...state, captcha }).then(res => {
        if (res && res.status < 350 && res.data) {
          setNotifier({ loading: false, message: res.data.msg });
          setCaptcha(false);
          setVerify(false);
          setState({ ...initialState });
        } else {
          setNotifier({
            loading: false,
            message:
              res.data.msg ||
              (res.data.errors && res.data.errors.map(v => v.msg).join()) ||
              "Something went wrong"
          });
          res.data.captcha && setCaptcha(true);
        }
      });
    } else {
      setNotifier({
        loading: false,
        message: "Please provide all required fields"
      });
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      {notifier.message && <Notify message={notifier.message} />}
      {notifier.loading && <LinearProgress />}
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign Up
        </Typography>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="name"
          label="Name"
          name="name"
          autoComplete="name"
          value={name}
          onChange={handleChange}
          autoFocus
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email Address"
          name="email"
          autoComplete="email"
          value={email}
          onChange={handleChange}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          autoComplete="current-password"
          value={password}
          onChange={handleChange}
        />
        {captcha && (
          <Recaptcha
            sitekey="6LcWyeUUAAAAAFLCcemnZ0Cf6NdqXQ1MTqri-Htt"
            render="explicit"
            verifyCallback={() => setVerify(true)}
            onloadCallback={() => console.log("Recaptcha Loaded")}
          />
        )}
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          className={classes.submit}
          onClick={handleSubmit}
          disabled={!name || !email || !password || (captcha && !verify)}
        >
          Sign Up
        </Button>
      </div>
    </Container>
  );
}

import React from "react";
import { shallow } from "enzyme";
import App from "./App";

describe("First test call", () => {
  it("Will success", () => {
    const component = shallow(<App />);
    const wrapper = component.find(`[data-test='AppTest']`);
    expect(wrapper.length).toBe(1);
  });
});

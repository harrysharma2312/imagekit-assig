import axios from "axios";

export function postSignupApi(data) {
  return axios
    .post("/user/signup", data, {
      headers: {
        "Content-Type": "application/json",
        "x-api-client": "OPS",
        "x-user-email": email
      }
    })
    .then(res => res)
    .catch(err => err.response);
}
